document.addEventListener("DOMContentLoaded", getLocation);
var city = document.getElementById("city"),
  temp = document.getElementById("temp"),
  anotherInfo = document.getElementById("another-info"),
  content = document.getElementById("content");
var longitude, latitude, interval, clear;
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
      longitude = position.coords.longitude;
      latitude = position.coords.latitude;
      showPosition();
    });
  } else {
    alert("Geolocation is not supported by this browser.");
  }
}

function showPosition() {
  interval = true;
  getWeatherInfo(longitude, latitude);
  clear = setInterval(() => {
    getWeatherInfo(longitude, latitude);
  }, 3000);
  window.onbeforeunload = function(event) {
    event.returnValue = "Write something clever here..";
  };
}

function getWeatherInfo(lon, lat) {
  fetch(
    "http://api.openweathermap.org/data/2.5/weather?lat=" +
      lat +
      "&lon=" +
      lon +
      "&APPID=a257e45511fb4f7812865f05038f6176",
    {
      method: "GET",
      headers: { "Content-Type": "text/plain" }
    }
  )
    .then(function(response) {
      return response.json();
    })
    .then(function(weather) {
      if (weather.cod === 200) {
        content.classList.remove("none");
        city.innerHTML = weather.name + ", " + weather.sys.country;
        temp.innerHTML =
          weather.weather[0].description +
          " <b>" +
          Math.round(weather.main.temp - 275) +
          " °C</b>";
        anotherInfo.innerHTML =
          "<p><b>Humidity:</b> " +
          weather.main.humidity +
          "%</p><p><b>Pressure: </b>" +
          weather.main.pressure +
          " hPa</p><p><b>Wind: </b>" +
          weather.wind.speed +
          " m/h</p><p><b>Clouds: </b>" +
          weather.clouds.all +
          "%</p>";
      } else {
        clearInterval(clear);
        alert("No information");
        getLocation();
      }
    })
    .catch(alert);
}

function stopInterval() {
  clearInterval(clear);
  interval = false;
  window.onbeforeunload = null;
}

function getNew() {
  if (interval) {
    if (confirm("Are you sure?")) {
      clearInterval(clear);
      longitude = document.getElementById("lon").value;
      latitude = document.getElementById("lat").value;
      showPosition();
    }
  } else {
    longitude = document.getElementById("lon").value;
    latitude = document.getElementById("lat").value;
    showPosition();
  }
}
